#!/usr/bin/env ruby
require 'discordrb'
require 'configatron'
require_relative 'config.rb'
require 'pp'

friday = "Gotta get down on Friday
Everybody's lookin' forward to the weekend, weekend
Friday, Friday
Gettin' down on Friday "

# bot = Discordrb::Bot.new token: configatron.token, client_id: configatron.client_id
bot = Discordrb::Commands::CommandBot.new token: configatron.token, client_id: configatron.client_id, prefix: '!'
if bot.invite_url
  puts "This bot's invite URL is #{bot.invite_url} - 10 seconds to activate it"
  10.times { ||
    print "."
    sleep 1
  }
end

bot.message(content: '!time') do |event|
  m = event.respond "The current time is: #{Time.now.strftime('%F %T %Z')}"
  m.delete
end

bot.command(:friday, description: 'Get down on friday') do |event|
  event.respond friday
end

bot.message(contains: 'friday') do |event|
  event.respond friday
end

bot.command(:connect) do |event|
  channel = event.user.voice_channel
  next "You're not in any voice channel!" unless channel
  bot.voice_connect(channel)
  "Connected to voice channel: #{channel.name}"
end

bot.command(:play_mp3) do |event|
  pp event
  channel = event.user.voice_channel
  puts "Channel: #{channel}"
  next "You're not in any voice channel!" unless channel
  bot.voice_connect(channel)
  event.respond "Connected to voice channel: #{channel.name}"
  voice_bot = event.voice
  voice_bot.play_file('data/rbf.mp3')
end

bot.message(content: 'Ping!') do |event|
  m = event.respond 'Pong!'
  m.edit "Pong! Time taken: #{Time.now - event.timestamp} seconds."
end

# This method call has to be put at the end of your script, it is what makes the bot actually connect to Discord. If you
# leave it out (try it!) the script will simply stop and the bot will not appear online.
bot.run
