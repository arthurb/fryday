Depenencies:
 - libsodium (+dev package)
 - opus (+dev package)
 - ffmpeg

Config
 - copy example.config.rb to example.conf
  + update the token and client_id to your api/bot

Running
Option 1.
bundle install && ./fryday.rb
Option 2.
docker build .
run the resulting container somewhere
