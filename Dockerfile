FROM alpine:latest
MAINTAINER Arthur Barton <arthurb@igniferous.net>

ENV APP_PACKAGE_DEP libsodium ffmpeg opus libsodium-dev opus-dev
ENV BUILD_PACKAGES curl-dev ruby-dev build-base ruby-rdoc libffi-dev ruby-irb
ENV RUBY_PACKAGES ruby ruby-bundler ruby-io-console

RUN echo 'gem: --no-rdoc --no-ri' > /etc/gemrc

# apk update
RUN apk update && \
  apk upgrade
# install packages
RUN apk add $APP_PACKAGE_DEP
RUN apk add $RUBY_PACKAGES $BUILD_PACKAGES
RUN apk add bash

# Clean cache
RUN rm -rf /var/cache/apk/*

# App
ENV WORKDIR /srv
COPY Gemfile $WORKDIR
COPY Gemfile.lock $WORKDIR
COPY . $WORKDIR
RUN cd $WORKDIR ; bundle install

CMD ["/srv/fryday.rb"]

